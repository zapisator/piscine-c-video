1
00:00:06,056 --> 00:00:10,688
Теперь, когда мы познакомились с теорией, перейдём к практике с примерами.

2
00:00:10,688 --> 00:00:14,444
Если вы позволите мне, я начну с примеров, которые не работают вообще.

3
00:00:14,444 --> 00:00:20,346
Поскольку я предпочёл бы совершить эти ошибки сам, перед вами,

4
00:00:20,346 --> 00:00:24,866
чтобы показать вам, как не надо делать, чем сделали бы их в своих проектах.

5
00:00:24,866 --> 00:00:29,501
Итак, я собираюсь применить то, что я показывал в предыдущем видео. Буквально.

6
00:00:29,501 --> 00:00:32,423
Я собираюсь создать рекурсивную функцию и называть её "fn()".

7
00:00:32,423 --> 00:00:35,743
Для того чтобы функция была рекурсивной, ей нужно вызывать себя.

8
00:00:35,743 --> 00:00:36,513
Так.

9
00:00:36,513 --> 00:00:37,483
Вот и всё.

10
00:00:37,483 --> 00:00:39,313
Я создал функцию самозапуска.

11
00:00:39,313 --> 00:00:43,103
Я собираюсь создать main, вызывающую fn().

12
00:00:43,103 --> 00:00:46,833
От этой программы не будет требоваться многое.

13
00:00:46,833 --> 00:00:49,043
Давайте добавим "return (0)", потому что мы цивилизованные.

14
00:00:49,043 --> 00:00:49,973
Та-ак.

15
00:00:49,973 --> 00:00:55,863
Я собираюсь скомпилировать это, а вы увидите, как мало будет выведено на экран, но зато будет кое-что что-то весёлое.

16
00:00:55,863 --> 00:00:58,763
Segfault! Но почему?

17
00:00:59,583 --> 00:01:04,353
Если вы были достаточно внимательны во время первого видео и держите в голове то, что я сказал 10 минут назад, то

18
00:01:04,353 --> 00:01:08,713
могли бы вспомнить, что функция требует условия остановки. В противном случае

19
00:01:08,713 --> 00:01:10,133
мы отправляем её в стек,

20
00:01:10,133 --> 00:01:11,563
она вызывает себя, мы отправляем в стек снова,

21
00:01:11,563 --> 00:01:14,533
и снова, и снова (x6) ...

22
00:01:14,533 --> 00:01:15,723
И в какой-то момент,

23
00:01:15,723 --> 00:01:15,973
ЭТО

24
00:01:15,973 --> 00:01:16,223
ВЗРЫВАЕТСЯ

25
00:01:16,223 --> 00:01:16,543
ПРЯМО

26
00:01:16,543 --> 00:01:16,793
НАМ

27
00:01:16,793 --> 00:01:17,043
В

28
00:01:17,043 --> 00:01:17,333
ЛИЦО

29
00:01:17,333 --> 00:01:20,353
Потому что мы превысили размер стека.

30
00:01:20,353 --> 00:01:21,753
Мы не должны этого делать.

31
00:01:21,753 --> 00:01:23,333
Вы будете удивлены, как часто это происходит.

32
00:01:23,333 --> 00:01:28,613
Так что теперь, мне интересно... Может быть, стоит добавить условие остановки так,

33
00:01:28,613 --> 00:01:33,433
чтобы моя функция не вызывала себя бесконечно.

34
00:01:33,433 --> 00:01:36,973
Может быть. Я не знаю. Начнём с добавления переменной.

35
00:01:36,973 --> 00:01:39,793
Я к этому привычный. Добавим переменную сюда.

36
00:01:39,793 --> 00:01:43,513
Затем инициализируем её 0, чтобы иметь счётчик.

37
00:01:43,513 --> 00:01:45,393
Затем поэкспериментируем.

38
00:01:45,393 --> 00:01:51,143
Если "i <= 5", тогда я вызваю "fn()".

39
00:01:51,143 --> 00:01:55,383
Иначе возвращаю 0.

40
00:01:55,383 --> 00:01:57,213
Хорошо?

41
00:01:57,213 --> 00:02:03,553
И перед вызовом fn() я собираюсь увеличить на 1 свой счётчик ("i"), хоть когда-то же надо.

42
00:02:03,553 --> 00:02:07,253
А то так и будет нулём, и моя забава никогда не будет работать.

43
00:02:07,253 --> 00:02:08,853
Давайте будем последовательными.

44
00:02:08,853 --> 00:02:11,523
Я собираюсь скомпилировать эту вещь. Он же должна работать, так ведь задумано?

45
00:02:12,433 --> 00:02:15,643
Хмммм... "Предупреждение". Это нормально.

46
00:02:16,233 --> 00:02:18,133
Нет, на самом деле это не работает.

47
00:02:18,133 --> 00:02:20,423
Это смешно, правда?

48
00:02:20,423 --> 00:02:23,703
Знаа-а-ачит... Что я мог сделать неправильно?..

49
00:02:23,703 --> 00:02:24,733
Точно!

50
00:02:24,733 --> 00:02:27,903
Посмотрите на функцию. Посмотрите внимательно.

51
00:02:27,903 --> 00:02:33,163
Я вхожу в свою функцию, я объявляю "i", я говорю "i = 0;", ладно.

52
00:02:33,163 --> 00:02:38,663
Я проверяю, "i <= 5". Ну, мы знаем, что это 0. Поэтому я увеличиваю его и вызываю fn().

53
00:02:38,663 --> 00:02:42,173
Потом я возвращаюсь в fn() для другого цикла, а та-а-ам

54
00:02:42,173 --> 00:02:47,443
Ну вот! Я объявляю переменную, присваиваю ей 0... Конечно, поэтому и не сработает!

55
00:02:47,443 --> 00:02:53,013
Потому помните: я положил fn() в стек, у неё есть свой собственный контекст и переменные,

56
00:02:53,013 --> 00:02:59,163
поэтому она получила свой "i", который равен 0, затем он увеличивается до 1, затем я вызываю fn(),

57
00:02:59,163 --> 00:03:05,073
Там я добавляю ещё однну fn() в стек, у которой есть свой собственный контекст и собственные переменные.

58
00:03:05,073 --> 00:03:06,763
Понимаете в чём дело?

59
00:03:06,763 --> 00:03:10,383
Итак, у моей второй fn() также есть свой "i", который установлен в 0, и теперь 1,

60
00:03:10,383 --> 00:03:15,213
и уж точно меньше 5, так что я снова вызываю. И снова, и снова...

61
00:03:15,213 --> 00:03:18,003
Понимаете к чему я клоню?

62
00:03:18,003 --> 00:03:20,553
Как  же мы можем это исправить?

63
00:03:20,627 --> 00:03:25,951
Помните основы Cи? Ту часть в которой передача параметров и возвращение значений?

64
00:03:25,951 --> 00:03:33,789
Вы знаете, что есть способ передать значения между функциями. Так что же мы можем сделать?

65
00:03:33,789 --> 00:03:39,459
Мы могли бы не объявлять здесь наш "i", а вместо этого передавать его как параметр!

66
00:03:40,048 --> 00:03:42,728
Мы можем и протестируем.

67
00:03:42,728 --> 00:03:47,798
На самом деле, я собираюсь добавить return(0), "предупреждение"! Разок - весело, больше - плохой вкус.

68
00:03:47,798 --> 00:03:51,798
Затем я передам свое настоящее "i" новому вызову fn().

69
00:03:52,062 --> 00:03:56,412
Я буду вызывать его с 0 в моей main(). Начать с 0.

70
00:03:56,412 --> 00:04:03,774
Так что у меня будет fn() с 0. Это меньше 5, так что я увеличу счётчик на 1,

71
00:04:05,005 --> 00:04:06,915
затем вызоваю fn() от значения 1.

72
00:04:07,132 --> 00:04:11,633
Так. У меня fn() от 1. Всё ещё меньше 5. Что мешает увеличить? Сделано.

73
00:04:11,633 --> 00:04:15,836
Снова вызов fn(), теперь от 2. Прилежно, которое я и хотел!

74
00:04:15,836 --> 00:04:26,192
Буду всячески показывать вам, что всё работает. Позвольте для начала добавить write() от «D» как в слове "Demo" (Демонстрация) или "Display" (Отображение, демонстрация)

75
00:04:26,192 --> 00:04:29,352
прямо вот тут, перед вызовом моей функции.

76
00:04:29,352 --> 00:04:31,972
Давайте скомпилируем.

77
00:04:31,972 --> 00:04:33,062
Ну вот же!

78
00:04:33,062 --> 00:04:35,442
Он отображает "D" шесть раз. Почему шесть?

79
00:04:35,442 --> 00:04:37,412
Потому что я пошел от 0 до 5. Включительно.

80
00:04:37,412 --> 00:04:38,752
Ну а это 6.

81
00:04:38,752 --> 00:04:43,632
Поскольку он отображает "D" шесть раз, это доказывает, что моя функция была вызвана ровно 6 раз.

82
00:04:43,632 --> 00:04:44,822
Это как раз я и хотел!

83
00:04:45,288 --> 00:04:51,528
Пришло время последнего примера, иллюстрирующий принципы вызова стека.

84
00:04:51,982 --> 00:04:53,972
Я просто добавлю ещё одну write().

85
00:04:53,972 --> 00:04:57,462
A write() от "F" как в "finished!" (Финиш). Сразу после вызова функции fn().

86
00:04:57,882 --> 00:05:03,588
И тут у нас два варианта: либо он будет отображать "DF" 6 раз,

87
00:05:03,588 --> 00:05:06,588
либо он отобразит что-то ещё.

88
00:05:06,588 --> 00:05:09,078
На ваш взгляд, что будет на экране?

89
00:05:09,163 --> 00:05:10,183
Увидим.

90
00:05:10,183 --> 00:05:13,733
Лично я не знаю. Узнаем вместе.

91
00:05:13,734 --> 00:05:15,194
Та-ак что будет отображаться?

92
00:05:15,227 --> 00:05:17,757
Ну, шесть раз "D" и шесть раз "F".

93
00:05:17,861 --> 00:05:19,471
Почему, как вы так думаете?

94
00:05:19,656 --> 00:05:21,116
Причина очень проста.

95
00:05:21,307 --> 00:05:27,857
В начале я "толкаю" свою fn() в стек, она выполняется до точки,

96
00:05:27,857 --> 00:05:31,687
там write(D). Затем вызов fn(). Что значит ровно следующее:  записать "D", затем вызвать fn().

97
00:05:31,687 --> 00:05:34,977
Второй вызов fn() пойдёт над первым, который ещё не закончен.

98
00:05:34,977 --> 00:05:36,187
Я добавляю один сверху.

99
00:05:36,187 --> 00:05:37,797
Я выполняю пока не отобразится "D".

100
00:05:37,797 --> 00:05:39,097
Я снова вызываю fn() и т.д.

101
00:05:39,097 --> 00:05:41,297
Я вывожу "D", вызываю fn() и т.д. и т.д.

102
00:05:41,297 --> 00:05:43,367
Через некоторое время я перестаю вызывать fn().

103
00:05:43,367 --> 00:05:44,237
Я иду назад.

104
00:05:44,727 --> 00:05:48,727
Я возвращаюсь к своей старой fn(), которая была вызвана другой fn(). Она заканчивась

105
00:05:48,812 --> 00:05:51,612
вызывает write(F). И write исполняется.

106
00:05:51,612 --> 00:05:55,362
Вот поэтому, после отображения всех Д-ешек, я теперь покажу F-ки, а затем return.

107
00:05:55,362 --> 00:05:58,622
Снова отображение "F" и снова return, отображение F - return и т. д. и т. д.

108
00:05:58,622 --> 00:06:03,312
Пока я не дойду до нижней части моей стопки вызвовов fn(), потом return переводит к main() и выход из программы.

109
00:06:03,312 --> 00:06:08,842
Вот так. Это пример рекурсивной функции. Делает-то чуть-чуть,

110
00:06:08,842 --> 00:06:14,552
но делает это хорошо. У неё есть условие остановки, которое работает.

111
00:06:14,554 --> 00:06:17,734
Она отображает то, что мы просим каких-либо хлопот.

112
00:06:17,734 --> 00:06:22,544
И если вы посмотрите внимательно, вы заметите, в конце концов, это похоже на цикл ...

113
00:06:22,544 --> 00:06:30,624
У меня есть "блоки инструкций" и счётчик. Мои инструкции повторяются.

114
00:06:30,624 --> 00:06:36,524
Рекурсия и итерация довольно таки разные.

115
00:06:36,524 --> 00:06:39,254
И с тем и с тем можно много чего делать.

116
00:06:39,254 --> 00:06:42,804
И при этом некоторые операции проще с итерацией,

117
00:06:42,804 --> 00:06:44,834
другие намного проще с рекурсией.

118
00:06:44,834 --> 00:06:47,464
Вы научитесь с опытом.

119
00:06:47,464 --> 00:06:51,224
Из этого видео важно запомнить две вещи.

120
00:06:51,224 --> 00:06:55,224
О рекурсивных функциях.

121
00:06:55,224 --> 00:06:56,594
Рекурсивная функция

122
00:06:56,594 --> 00:06:59,134
1) функция, которая вызывает себя;

123
00:06:59,134 --> 00:07:02,954
2) она имеет условие остановки.

124
00:07:02,954 --> 00:07:05,904
(особенно если вы не хотите, чтобы что взрывалось прямо вам в лицо)

125
00:07:05,904 --> 00:07:11,814
И если вы используете счётчики. Обязательно передавайте их между экземплярами каждой функции.

126
00:07:11,814 --> 00:07:14,964
(параметры и возвращаемые значения)

127
00:07:14,964 --> 00:07:20,434
Это все на сегодня! Так что удачи на сегодняшних упражнениях.

128
00:07:20,434 --> 00:07:21,934
Тебе она понадобится!

129
00:07:21,934 --> 00:07:23,384
До следующего раза!
